<?php include 'db_connection.php';?>
<?php include 'head.php';?>
<?php include 'isset_login.php';?>

<body>
<form method="POST" action="index.php">
  <div class="form-group">
    <label for="email">Email</label>
    <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Email">
  </div>
  <div class="form-group">
    <label for="username">Utilisateur</label>
    <input type="user" class="form-control" id="username" name="username" aria-describedby="userName" placeholder="Utilisateur">
  </div>
  <div class="form-group">
    <label for="password">Password</label>
    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
  </div>
  <button type="submit" class="btn btn-primary">Valider</button>
  <input type="hidden" name="login" value="1"> 
</form>
</body>

<?php include 'footer.php';?>